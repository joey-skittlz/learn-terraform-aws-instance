#!/bin/bash

sudo apt-get update

sudo apt-get -y install ca-certificates 
sudo apt-get -y install curl
sudo apt-get -y install gnupg 
sudo apt-get -y install lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get -y install docker-ce 
sudo apt-get -y install docker-ce-cli 
sudo apt-get -y install containerd.io
