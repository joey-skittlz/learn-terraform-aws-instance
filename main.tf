terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.47.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

variable "instance_name" {
  default = "observe_demo_server"
}

resource "aws_instance" "app_server" {
  key_name               = aws_key_pair.deployer.id
  ami                    = "ami-0fb653ca2d3203ac1"
  instance_type          = "t2.2xlarge"
  vpc_security_group_ids = ["sg-03a8cc3b92f6abd77"]

  root_block_device {
    volume_size = "240"
    volume_type = "gp2"
  }

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/terraform/welsh_account_key_pair.cer")
    host        = self.public_ip
  }

  provisioner "file" {
    source      = "install_script.sh"
    destination = "/tmp/install_script.sh"
  }

  provisioner "file" {
    source      = "install_script-2.sh"
    destination = "/tmp/install_script-2.sh"
  }

  provisioner "file" {
    source      = "install_script-3.sh"
    destination = "/tmp/install_script-3.sh"
  }

#Install Docker
  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install_script.sh",
      "/tmp/install_script.sh",
    ]
  }

 /* provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install_script-2.sh",
      "/tmp/install_script-2.sh",
    ]
  }

    provisioner "remote-exec" {
    inline = [
      "minikube start",
      "sudo usermod -aG docker $USER && newgrp docker",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "chmod +x /tmp/install_script-3.sh",
      "/tmp/install_script-3.sh",
    ]
  }*/

  tags = {
    Name = var.instance_name
  }

}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCd/EhxcW2PcO8aW4320FU7IDnfJwxNoN8MvR0DiaFWQudzF1/thtVMDpO7514q2FzsW1Cq8KH/u0NJahCD9d5bUFlLMwFwFp8ZGMpkLUpDyALK3xvDmZowpt8xHIxsRviNRiSYB5les2Ge8MBB4yq3VNyQUHh/naLB8ij5KSaKgxLQvC4pIK8wiy3fqCxXVwND5Qrk9s4WeMB2SbKLQboxmqsLkYvCrEB1Wh4OlREGUzsuqYTg/t0g8nkgCNdg1dpM4qf72HtEiT+WGu/d0Z1ovQGDgUcllK63RrgmBVyg1v3pVzFeo64W4v3mbKzAZczmeYMdDYvcnAEjxdNfDool welsh_account_key_pair"
}
