terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.47.0"
    }
  }
}

provider "aws" {
  region = "us-east-2"
}

variable "instance_name" {
  default = "k8_and_jenkins"
}

resource "aws_volume_attachment" "ebs_att" {
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.app_server_storage.id
  instance_id = aws_instance.app_server.id
}


resource "aws_instance" "app_server" {

  key_name               = aws_key_pair.deployer.id
  ami                    = "ami-0fb653ca2d3203ac1"
  instance_type          = "t2.2xlarge"
  vpc_security_group_ids = ["sg-03a8cc3b92f6abd77"]

  connection {
    type        = "ssh"
    user        = "ubuntu"
    private_key = file("~/.ssh/terraform/welsh_account_key_pair.cer")
    host        = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get install ca-certificates curl gnupg lsb-release",
      "curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg",
      "echo \"deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable\" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null",
      "sudo apt-get update",
      "sudo apt-get install -y docker-ce docker-ce-cli containerd.io",
      "sudo usermod -aG docker $USER && newgrp docker",
      "curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64",
      "sudo install minikube-linux-amd64 /usr/local/bin/minikube",
      "minikube start",
      "curl -LO \"https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl\"",
      "sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl",
      "kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4",
      "kubectl expose deployment hello-minikube --type=NodePort --port=8080",
      "kubectl get services hello-minikube"
    ]
  }

  tags = {
    Name = var.instance_name
  }

}

resource "aws_ebs_volume" "app_server_storage" {
  availability_zone = "us-east-2a"
  size              = 250
}

resource "aws_key_pair" "deployer" {
  key_name   = "deployer_key"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCd/EhxcW2PcO8aW4320FU7IDnfJwxNoN8MvR0DiaFWQudzF1/thtVMDpO7514q2FzsW1Cq8KH/u0NJahCD9d5bUFlLMwFwFp8ZGMpkLUpDyALK3xvDmZowpt8xHIxsRviNRiSYB5les2Ge8MBB4yq3VNyQUHh/naLB8ij5KSaKgxLQvC4pIK8wiy3fqCxXVwND5Qrk9s4WeMB2SbKLQboxmqsLkYvCrEB1Wh4OlREGUzsuqYTg/t0g8nkgCNdg1dpM4qf72HtEiT+WGu/d0Z1ovQGDgUcllK63RrgmBVyg1v3pVzFeo64W4v3mbKzAZczmeYMdDYvcnAEjxdNfDool welsh_account_key_pair"
}
